﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SurveyManager.DataAccessLayer;
using SurveyManager.Models;

namespace SurveyManager.Controllers
{
    public class HomeController : Controller
    {
        readonly FeedbackContext context;

        public HomeController(FeedbackContext fcontext)
        {
            context = fcontext;
        }

        public async Task<IActionResult> Index()
        {
            using (HttpClient cli = new HttpClient())
            {
                HttpResponseMessage response = await cli.GetAsync("http://localhost:49789/api/surveys");
                List<Survey> surveys = new List<Survey>();
                if (response.IsSuccessStatusCode)
                {
                    var surveysSerialized = await response.Content.ReadAsStringAsync();
                    if (surveysSerialized == "") return View();
                    surveys = JsonConvert.DeserializeObject<List<Survey>>(surveysSerialized);
                    SurveyListViewModel model = new SurveyListViewModel();
                    model.items = surveys;
                    return View(model);
                }
                return View();
            }
        }
        [HttpGet]
        public IActionResult CreateSurvey()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateSurvey(Survey survey)
        {
            using (HttpClient cli = new HttpClient())
            {
                survey.CreatedDate = DateTime.Now;
                StringContent content = new StringContent(JsonConvert.SerializeObject(survey), Encoding.UTF8, "application/json");
                await cli.PostAsync("http://localhost:49789/api/survey", content);
                return RedirectToAction("Index");
            }
        }
        [HttpGet]
        public IActionResult CreateQuestion()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateQuestion(DataAccessLayer.Question question)
        {
            HttpClient cli = new HttpClient();
            question.CreatedDate = DateTime.Now;
            StringContent content = new StringContent(JsonConvert.SerializeObject(question), Encoding.UTF8, "application/json");
            await cli.PostAsync("http://localhost:49789/api/question", content);
            return RedirectToAction("Index");
        }
        [HttpGet("Home/Question/{id}")]
        public async Task<IActionResult> Question(int id)
        {
            SurveyViewModel model = new SurveyViewModel();
            List<QuestionViewModel> questModelList = new List<QuestionViewModel>();
            List<Question> questions = new List<Question>();
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage responseSurvey = await client.GetAsync("http://localhost:49789/api/survey/" + id.ToString());
                var surveySerialized = await responseSurvey.Content.ReadAsStringAsync();
                model.item = JsonConvert.DeserializeObject<Survey>(surveySerialized);
                HttpResponseMessage response = await client.GetAsync("http://localhost:49789/api/surveyquestions/" + id.ToString());
                var questionsSerialized = await response.Content.ReadAsStringAsync();
                if (questionsSerialized != "")
                    questions = JsonConvert.DeserializeObject<List<Question>>(FixApiResponseString(questionsSerialized));
                foreach (var item in questions)
                {
                    QuestionViewModel questModel = new QuestionViewModel();
                    questModel.item = item;
                    HttpResponseMessage responseResponses = await client.GetAsync("http://localhost:49789/api/question/" + id.ToString());
                    var responsesSerialized = await responseResponses.Content.ReadAsStringAsync();
                    questModel.responses = JsonConvert.DeserializeObject<KeyValuePair<Question, List<Response>>>(FixApiResponseString(responsesSerialized)).Value;
                    questModelList.Add(questModel);
                }
                model.questions = questModelList;
                return View(model);
            }
        }
        public string FixApiResponseString(string input)
        {
            input = input.Replace("\\", string.Empty);
            input = input.Trim('"');
            return input;
        }
    }
}
