﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SurveyManager.DataAccessLayer;

namespace SurveyManager.Controllers
{
    [Route("api/")]
    public class SurveyController : Controller
    {
        readonly FeedbackContext context;

        public SurveyController(FeedbackContext db)
        {
            context = db;
        }

        [Route("surveys/")]
        // GET: api/surveys
        [HttpGet]
        public string Get()
        {
            var surveys = context.Surveys.ToList();
            if (!surveys.Any()) return "";
            var serializedSurveys = Newtonsoft.Json.JsonConvert.SerializeObject(surveys);
            //List all surveys
            return serializedSurveys;
        }
        // GET: api/Survey/5
        [HttpGet("survey/{id}", Name = "GetSurvey")]
        public string Get(int id)
        {
            var survey = context.Surveys.FirstOrDefault(s => s.Id == id);
            if (survey == null) return "";
            var serializedSurvey = Newtonsoft.Json.JsonConvert.SerializeObject(survey);
            //List all surveys
            return serializedSurvey;
        }
        [Route("survey/")]
        // POST: api/survey
        [HttpPost]
        public void Post([FromBody]Survey survey)
        {
            context.Surveys.Add(survey);
            context.SaveChanges();
        }
        //POST: api/survey/5
        [HttpPost("{id}")]
        public void Post(int id, [FromBody]Question question)
        {
            question.SurveyID = id;
            context.Questions.Add(question);
            context.SaveChanges();
        }
        [Route("survey/")]
        // PUT: api/Survey/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Survey survey)
        {
            var editingSurvey = context.Surveys.FirstOrDefault(s => s.Id == id);
            if (editingSurvey == null) return;
            context.Surveys.Update(editingSurvey);
            editingSurvey.Name = survey.Name;
            editingSurvey.CreatedDate = survey.CreatedDate;
            editingSurvey.CreatorName = survey.CreatorName;
            editingSurvey.Text = survey.Text;
            context.SaveChanges();
        }
        [Route("survey/")]
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var questions = context.Questions.Where(s => s.SurveyID == id);
            if (questions != null) foreach (var question in questions)
                {
                    var responses = context.Responses.Where(s => s.QuestionID == question.Id);
                    if (responses != null) foreach (var item in responses) context.Responses.Remove(item);
                    context.Questions.Remove(question);
                }
            var survey = context.Surveys.FirstOrDefault(s => s.Id == id);
            if(survey != null) context.Surveys.Remove(survey);

            context.SaveChanges();
        }
    }
}
