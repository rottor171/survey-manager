﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SurveyManager.DataAccessLayer;

namespace SurveyManager.Controllers
{
    [Produces("application/json")]
    [Route("api/")]
    public class SurveyQuestionsController : Controller
    {
        readonly FeedbackContext context;

        public SurveyQuestionsController(FeedbackContext db)
        {
            context = db;
        }

        [HttpGet("surveyquestions/{id}", Name = "GetSurveyQuestions" )]
        public string Get(int id)
        {
            var questions = context.Questions.Where(q => q.SurveyID == id).ToList();
            var serializedQuestions = Newtonsoft.Json.JsonConvert.SerializeObject(questions);
            return serializedQuestions;
        }
        [HttpDelete("surveyquestions/{id}")]
        public void Delete(int id)
        {
            var questions = context.Questions.Where(s => s.SurveyID == id);
            if (questions != null) foreach (var question in questions) context.Questions.Remove(question);
            context.SaveChanges();
        }
    }
}
