﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SurveyManager.DataAccessLayer;

namespace SurveyManager.Controllers
{
    [Route("api/")]
    public class QuestionsController : Controller
    {
        readonly FeedbackContext context;

        public QuestionsController(FeedbackContext db)
        {
            context = db;
        }

        // GET: api/Questions
        [Route("questions/")]
        [HttpGet]
        public string Get()
        {
            var questions = context.Questions.ToList();
            var serializedQuestions = Newtonsoft.Json.JsonConvert.SerializeObject(questions);
            //List all surveys
            return serializedQuestions;
        }
        // GET: api/Questions/5
        [HttpGet("question/{id}", Name = "GetQuestion")]
        public string Get(int id)
        {
            var question = context.Questions.FirstOrDefault(s => s.Id == id);
            if (question == null) return "";
            var responses = context.Responses.Where(s=>s.QuestionID == id).ToList();
            KeyValuePair<Question, List<Response>> quest = new KeyValuePair<Question,List<Response>>(question, responses);
            var serializedquestion = Newtonsoft.Json.JsonConvert.SerializeObject(quest);
            //List all questions
            return serializedquestion;
        }
        [Route("question/")]
        // POST: api/Questions
        [HttpPost]
        public void Post([FromBody]Question question)
        {
            var responseTrue = new Response();
            var responseFalse = new Response();
            responseTrue.Correctness = true;
            responseTrue.Text = "True";
            responseTrue.QuestionID = question.Id;
            responseTrue.Id = context.Responses.Count() + 1;
            context.Responses.Add(responseTrue);
            responseFalse.Correctness = false;
            responseFalse.Text = "False";
            responseFalse.QuestionID = question.Id;
            responseFalse.Id = context.Responses.Count() + 1;
            context.Responses.Add(responseFalse);
            context.Questions.Add(question);
            context.SaveChanges();
        }
        [Route("question/")]
        // PUT: api/Questions/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]Question question)
        {
            var editingQuestion = context.Questions.FirstOrDefault(s => s.Id == id);
            if (editingQuestion == null) return;
            context.Questions.Update(editingQuestion);
            editingQuestion.Name = question.Name;
            editingQuestion.CreatedDate = question.CreatedDate;
            editingQuestion.Text = question.Text;
            context.SaveChanges();
        }
        [Route("question/")]
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            var responses = context.Responses.Where(s => s.QuestionID == id);
            if (responses != null) foreach (var item in responses) context.Responses.Remove(item);

            var question = context.Questions.FirstOrDefault(s => s.Id == id);
            if (question != null) context.Questions.Remove(question);

            context.SaveChanges();
        }
    }
}
