﻿using SurveyManager.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SurveyManager.Models
{
    public class QuestionViewModel
    {
        public Question item { get; set; } = new Question();
        public List<Response> responses { get; set; } = new List<Response>();
    }
}
