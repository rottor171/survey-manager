﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SurveyManager.Models
{
    public class SurveyViewModel
    {
        public DataAccessLayer.Survey item { get; set; } = new DataAccessLayer.Survey();
        public List<QuestionViewModel> questions { get; set; } = new List<QuestionViewModel>();
    }
}
