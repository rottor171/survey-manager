﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SurveyManager.Models
{
    public class SurveyListViewModel
    {
        public List<DataAccessLayer.Survey> items { get; set; } = new List<DataAccessLayer.Survey>();
    }
}
