﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace SurveyManager.DataAccessLayer
{
    [JsonObject]
    public class Survey
    {
        [JsonProperty]
        public int Id { get; set; }
        [JsonProperty]
        public string Name { get; set; }
        [JsonProperty]
        public string Text { get; set; }
        [JsonProperty]
        public string CreatorName { get; set; }
        [JsonProperty]
        public DateTime CreatedDate { get; set; }
    }
    [JsonObject]
    public class Question
    {
        [JsonProperty]
        public int Id { get; set; }
        [JsonProperty]
        public string Name { get; set; }
        [JsonProperty]
        public string Text { get; set; }
        [JsonProperty]
        public DateTime CreatedDate { get; set; }
        [JsonProperty]
        public int SurveyID { get; set; }
    }
    [JsonObject]
    public class Response
    {
        [JsonProperty]
        public int Id { get; set; }
        [JsonProperty]
        public bool Correctness { get; set; }
        [JsonProperty]
        public string Text { get; set; }
        [JsonProperty]
        public int QuestionID { get; set; }
    }
    public class FeedbackContext : DbContext
    {
        public DbSet<Survey> Surveys { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Response> Responses { get; set; }
        public FeedbackContext(DbContextOptions<FeedbackContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
